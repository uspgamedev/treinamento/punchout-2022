extends Control


export var damage := 20

# Called when the node enters the scene tree for the first time.
func _ready():
	$Button.connect("pressed", $TextureProgress, "_on_damage_taken", [20])
