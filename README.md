# Punch-out 2022 (nome sujeito a mudanças)

Projeto de treinamento.

Participantes:
- TODO
- Renan Ribeiro Marcelino
- Karina Ribeiro
- Fernando Yang 
=======

Tutor:
- @Kazuo256 (também conhecido como Wil)

## Como usar Git no Windows

Segue aqui um passo-a-passo bem superficial de como usar Git no Windows.

### Passo 1: Instalar VisualStudio Code (ou equivalente)

Parte de usar Git (e desenvolver jogos, em geral) envolve escrever texto em
arquivos de "texto puro", como \*.txt ou \*.md (vide este arquivo). Para isso,
o bloco de notas padrão do Windows é muito limitado e acaba mais atrapalhando
do que ajudando. O recomendado é ter um editor de texto dedicado, e um dos mais
comumente recomendados é o VSCodium (pode ser abaixado
[aqui](https://github.com/VSCodium/vscodium/releases/download/1.66.2/VSCodium-x64-1.66.2.msi)),
mas outras opções são o Atom, o Notepad++, e outros.

Abaixe e instale o que preferir. Fique à vontade para pedir ajuda se tiver
qualquer dificuldade. Se você escolher o VSCodium, preste atenção nos passos da
instalação para deixar a opção "Add to PATH" selecionada!

### Passo 2: Instalar o Git for Windows

[Abaixe o instalador](https://github.com/git-for-windows/git/releases/download/v2.36.0.windows.1/Git-2.36.0-64-bit.exe)
e instale no seu computador. Tem váaarios passos de configuração, alguns
bastante misteriosos. Qualquer dúvida, pergunte e/ou peça ajuda de alguém no
servidor do UGD.

Agora, vá até uma pasta no navegador de arquivo (por exemplo, a que você
gostaria de usar para guardar o projeto) e clique com o botão direito do mouse
em um espaço vazio. Procure por uma opção "Open in Git BASH" ou similar.
Selecione-a e confira se um console aparece.

**Esse é o Git BASH e é por ele que você vai usar comando de Git para registrar
o histórico de mudanças e sincronizar os arquivos do projeto**. Use ele para
realizar os passos seguintes.

### Passo 3: Configurar o Git

Tem três coisas para configurar: a autora padrão de commits, o editor de texto
padrão de commits e uma chave SSH.

#### Autora padrão de commit

Escreva os seguinte comandos no console e pressione ENTER no final para
executá-los. Computadores são burros, então você tem que acertar caracter por
caracter a linha! Só lembre de trocar as partes relevantes pelos seus dados:

```bash
git config --global user.name "Meu Nome"
git config --global user.email "meu@email.com"
```

Agora todos os registros (commits) que você ficar com seu git vão incluir seus
dados para que possamos rastrear o histórico de mudanças de maneira completa.

#### Editor de texto padrão

Se você escolheu o VSCodium, só rodar o seguinte comando:

```bash
git config --global core.editor "code --wait"
```

Se não, melhor pesquisar na internet como fazer no seu caso, ou pedir ajuda
para alguém no UGD.

#### Chave SSH

Chaves SSH são a forma que o Git usa para autenticar os autores dos commits de
forma segura. Quando você cria uma chave, ela tem três partes:

1. Uma chave pública (arquivo `rsa.pub`)
2. Uma chave privada (arquivo `rsa`)
3. Uma senha que só você sabe

Comece pelo comando:

```bash
ssh-keygen -t rsa -b 2048 -C "meu@email.com"
```

Isso iniciará um processo com vários passos. Cada passo vai te fazer uma
pergunta que você responde digitando a resposta e dando ENTER depois.

Na primeira pergunta (onde salvar a chave), simplesmente aperte ENTER sem
digitar nada. Desse jeito, ele usa a localização padrão, que é na *home* do seu
usuário.

Na segunda e terceira perguntas, você terá que criar uma senha e repetí-la,
respectivamente. Note que quando você digitar *os caracteres não vão aparecer
no console*. Isso é a forma dele "esconder" sua senha caso alguém esteja te
espiando :eyes:.

Confirmada sua senha, abra seu navegador de arquivos e vá para a pasta *home*
do seu usuário. Isso costuma ser algo do tipo `C:\Users\meunome`. Lá deve ter
uma pasta `.ssh`. Dentro dela, vão estar os arquivos com suas chaves. Nós
queremos apenas sua chave pública (`rsa.pub`), a privada *nunca deve ser
compartilhada com ninguém*. Abra a chave pública no seu novo editor de texto e
copie todo o conteúdo.

Vá para as configurações de chaves SSH na sua conta do GitLab ([aqui](https://gitlab.com/-/profile/keys))
e cole o que você copiou no campo "Key". Coloque em "Title" um nome para você
lembrar que essa chave está guardada neste computador (você pode ter várias
espalhadas em diversas máquinas). Não coloque data de expiração. Clique em
"Add key".

Pronto! Seu Git está configurado!

### Passo 4: clonar este repositório

Abra seu console Git BASH na pasta onde quer que fique o projeto. Use o
seguinte comando:

```bash
git clone git@gitlab.com:uspgamedev/treinamento/punchout-2022.git
```

Vão aparecer umas mensagens e uma nova pasta chamada `punchout-2022` vai ser
criada com este projeto dentro dela. Pronto! Agora é só abrir o projeto na
Godot e por a mão na massa!

## Colinha dos comandos do Git

```
git status
```

Lista as mudanças detectadas localmente com relação ao último commit.

```
git diff [--cached]
```

Mostra, em detalhes, linha por linha, o que você mudou com relação ao último
commit. A opção `--cached` serve para mostrar as mudanças que estão preparadas
para entrar em commit (isto é, foram adicionadas com `git add`).

```
git add <arquivo 1> <arquivo 2> [...]
```

Prepara arquivos com mudanças para serem incluídos no próximo commit que for
feito.

```
git commit [-m "mensagem de commit"]
```

Salva as mudanças selecionadas com `git add` em um commit e anexa uma mensagem
para consulta posterior. A opção `-m` permite especificar a mensagem de commit
na mesma linha que o comando. Sem ela, o git tenta abrir um editor de texto
para você elaborar a mensagem.

```
git log
```

Lista o histórico de commits do mais recente para o mais antigo.

```
git pull
```

Traz as mudanças presentes no GitLab para a sua timeline local.

```
git push
```

Faz upload dos commits novos que você fez localmente, mandando-os para o
GitLab.

