extends Node2D

signal attack_dealt

signal damage_taken(health_percent)

var health = 100

var is_vulnerable = true
var can_move = true

const DAMAGE_PER_HIT = 8

func _unhandled_input(event):
	if event.is_action_pressed("click"):
		begin_attack()

	if event.is_action_pressed("right_click"):
		get_hit()

func begin_attack():
	if not can_move:
		return

	is_vulnerable = false
	can_move = false
	$AnimationPlayer.play("Attack")
	
	

func on_attack_hint():
	$HintAudioPlayer.play()

func on_attack_damage_time_began():
	$AttackAudioPlayer.play()
	emit_signal("attack_dealt")


func on_attack_end():
	can_move = true
	is_vulnerable = true
	print("Enemy Attack End")

func get_hit():
	if not is_vulnerable:
		return

	print("Enemy Got Hit")
	
	health -= DAMAGE_PER_HIT
	
	emit_signal("damage_taken", health)

	can_move = false
	$AnimationPlayer.stop()
	$AnimationPlayer.play("GetHit")

func on_hit_end():
	can_move = true

func disable_movement():
	can_move = false
