extends Control

var message: String

func set_message(given):
	message = given

func _ready():
	$Label.text = message


func _on_Button_pressed():
	
	# The scene will not only exists in memoy, but will render and retain
	# its behavior if we just change the scene.
	
	queue_free()
	get_tree().change_scene_to(load("res://src/Menu/Menu.tscn"))
