extends Node


var result_scene = preload("res://src/Result/Result.tscn").instance()


func _on_Enemy_attack_dealt():
	$Player.get_hit()


func _on_Player_attack_dealt():
	$Enemy.get_hit()


func _on_Enemy_damage_taken(health_percent):
	if health_percent < 0:
		end("Victory")


func _on_Player_damage_taken(health_percentage):
	if health_percentage < 0:
		end("Game Over")
		
func end(message):
	result_scene.set_message(message)
	get_parent().add_child(result_scene)
	$Player.disable_movement()
	$Enemy.disable_movement()
	
	queue_free()
