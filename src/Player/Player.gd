extends Node2D

signal attack_dealt

signal damage_taken(health_percentage)

var current_health = 100

const DAMAGE_PER_HIT = 8

var movement_allowed = true
var can_move = true
var is_vulnerable = true

func _unhandled_input(event):
	if event.is_action_pressed("player_dodge_right"):
		begin_dodge("Dodge Right")
		
	if event.is_action_pressed("player_dodge_left"):
		begin_dodge("Dodge Left")
		
	if event.is_action_pressed("player_attack"):
		begin_attack()

func begin_dodge(animation):

	if not can_move || not movement_allowed:
		return
	
	print("Player Dodge - ", animation)

	can_move = false
	is_vulnerable = false

	$AnimationPlayer.stop()
	$AnimationPlayer.play(animation)
	
func begin_attack():

	if not can_move || not movement_allowed:
		return

	print("Player Attack")

	can_move = false

	$AnimationPlayer.stop()
	$AnimationPlayer.play("Attack")
	
	$AttackAudioPlayer.play()

func _on_attack_damage_activated():
	print("Player Attack Damage")
	emit_signal("attack_dealt")

func _on_end_attack():
	print("Player End Attack")
	can_move = true
	
func get_hit():

	if not is_vulnerable:
		return
		
	current_health -= DAMAGE_PER_HIT
		
	emit_signal("damage_taken", current_health)

	can_move = false

	print("Player Got hit")
	$AnimationPlayer.stop()
	$AnimationPlayer.play("GetHit")


func _on_end_dodge():
	print("Player End Dodge")
	can_move = true
	is_vulnerable = true


func _on_end_hit():
	print("Player End Hit")
	can_move = true

func disable_movement():
	movement_allowed = false
